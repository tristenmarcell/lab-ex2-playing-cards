#include <conio.h>
#include <iostream>
#include <string>

using namespace std;


enum Suit 
{
	//Ordered so that standard convention of using alphabetical order can be applied 
	// Clubs < Diamonds < Hearts < Spades in case of rank tie - ace of spades beats ace of clubs)
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES

};

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card 
{
	Suit suit;
	Rank rank;
};

void PrintCard(Card card) {
	std::cout << "The ";

	if (card.rank == 11) {
		std::cout << "Jack";
	}
	else if (card.rank == 12) {
		std::cout << "Queen";
	}
	else if (card.rank == 13) {
		std::cout << "King";
	}
	else if (card.rank == 14) {
		std::cout << "Ace";
	}
	else {
		std::cout << card.rank;
	};

	std::cout << " of ";

	if (card.suit == 0) {
		std::cout << "Clubs.\n";
	}
	else if (card.suit == 1) {
		std::cout << "Diamonds.\n";
	}
	else if (card.suit == 2) {
		std::cout << "Hearts.\n";
	}
	else if (card.suit == 3) {
		std::cout << "Spades.\n";
	}
};

Card HighCard(Card card1, Card card2) {
	if (card1.rank >= card2.rank) {
		return card1;
	}
	else {
		return card2;
	}
};

int main()
{
	Card card1;
	card1.rank = ACE;
	card1.suit = SPADES;

	Card card2;
	card2.rank = TEN;
	card2.suit = HEARTS;

	PrintCard(HighCard(card1, card2));

	_getch();
	return 0;
}